function ATA = solve_sedumi(m, neq, eqs, rhs);

% %%% Add the SeDuMi directory files %%%%%%%%%
% Separator = '/';
% BasePath  = [pwd Separator];
% 
% fprintf('DGPaths.m: adding SeDuMi paths ...\n');
% 
% addpath([BasePath 'sedumi']);
% fprintf('\t%s\n', [BasePath 'sedumi']);
% 
% fprintf('\n');
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% Find the best PSD matrix X that minimizes || eqs * X - rhs ||
% (hopefully X=PTP ...)
A = zeros(m^2);
b = zeros(1,m^2);

for i=1:neq;
    A = A + eqs(i,:)' * eqs(i,:);
    b = b + 2 * rhs(i) * eqs(i,:);
end;

% DEBUG: the following should be 0
% PTP_vec' * A * PTP_vec - b*PTP_vec + rhs'*rhs

% Minimize: X'*A*X - b*X, or:
% with X psd

% Dummy variable l >= X'*A'X 
% Minimize l - b*X
% with X psd
% and [I sqrt(A)*X ; (sqrt(A)*X)' l] psd

% Number of variables: m^2 (for X) + (m^2+1)^2 (for the second constraint -- slack variables)
c = zeros(m^2 + (m^2+1)^2, 1);
c(1:m^2) = -b';
c(m^2 + (m^2+1)^2) = 1; % for l

% Number of equations: (m^2+1)^2 - 1 (we don't need an equation for l)
% sparsity:
% The Id block gives m^4 eqs with 1 var: m^4
% The sqrt(A)*X gives twice m^2 eqs with (m^2+1) vars: 2*m^2*(m^2+1)
% Total: 3*m^4+2*m^2 (very sparse!)

% construct equations for dummy (slack) variables
I = zeros(3*m^4+2*m^2, 1);
J = zeros(3*m^4+2*m^2, 1);
val = zeros(3*m^4+2*m^2, 1);
rhs_constr = zeros((m^2+1)^2 - 1,1);

% First do the m^2 x m^2 Id block
idx = 0;
for i=1:m^2;
    for j=1:m^2;
        idx = idx + 1;
        I(idx) = idx;
        J(idx) = m^2 + (m^2+1)*(i-1) + j;
        val(idx) = 1;
        if (i == j)
            rhs_constr(idx) = 1;
        end;
    end;
end;

idx0 = idx;

sqrt_A = A^(1/2);

for i=1:m^2;
    idx0 = idx0 + 1;
    for j=1:m^2;
        idx = idx + 1;
        I(idx) = idx0;
        J(idx) = j;
        val(idx) = sqrt_A(i,j);
    end;
    idx = idx + 1;
    I(idx) = idx0;
    J(idx) = m^2 + (m^2+1)*(i-1) + m^2+1;
    val(idx) = -1;
end;

for i=1:m^2;
    idx0 = idx0 + 1;
    for j=1:m^2;
        idx = idx + 1;
        I(idx) = idx0;
        J(idx) = j;
        val(idx) = sqrt_A(i,j);
    end;
    idx = idx + 1;
    I(idx) = idx0;
    J(idx) = m^2 + (m^2+1)*m^2 + i;
    val(idx) = -1;
end;

eqs_constr = sparse(I,J,val, (m^2+1)^2 - 1, m^2 + (m^2+1)^2);

K.s = [m, m^2+1];

pars.fid = 0;

[x,y,info] = sedumi(eqs_constr, rhs_constr, c, K, pars);

ATA = mat(x(1:m^2),m);
