function r_embed = sensor_uscities2(k,n_eigs,noise,n_use);

load uscities.mat;

%%% Add the SeDuMi directory files %%%%%%%%%
Separator = '/';
BasePath  = [pwd Separator];

fprintf('DGPaths.m: adding SeDuMi paths ...\n');

addpath([BasePath 'sedumi']);
fprintf('\t%s\n', [BasePath 'sedumi']);

fprintf('\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x_data = uscities(1,:)';
y_data = uscities(2,:)';

N = length(x_data);

% % Plot data points
% scatter(x_data, y_data, 5);
% xlabel('x');
% ylabel('y');
% title('US Cities');

r_data = [x_data y_data]';

dist = zeros(N);
for j=1:N;
    dist(:,j) = (x_data(:)-x_data(j)).^2 + (y_data(:)-y_data(j)).^2; 
end;

noise_matrix = 1/sqrt(2) * noise*randn(N);
noise_matrix = noise_matrix + noise_matrix';
for i=1:N;
    noise_matrix(i,i) = 0;
end;

noisy_dist = dist .* ((ones(N) + noise_matrix).^2);
    
% Find nearest neighbors
kdt = ann_kdtree(r_data);

% dists are the real 'clean' distances of the nearest neighbors
[idxs, dists] = ann_nn(kdt, r_data, k+1);

% add noise to dists 
for i=1:N;
    dists(:,i) = noisy_dist(idxs(:,i),i);    
end;

R = zeros(3,k);
b = [0; 0; 1];

I = zeros(N*k,1);
J = zeros(N*k,1);
values = zeros(N*k,1);

index = 1:k;

for i=1:N;
    % matlab's cmdscale performs MDS to get coordinates from noisy distances
    % it does triangulation, up to rotation and perhaps reflection
    [Y,e] = cmdscale(sqrt(noisy_dist(idxs(:,i),idxs(:,i))));
    
    % we take only the first two coordinates, because we are in the plane
    % we also center so that the first point is the new origin
    R(1,:) = Y(2:(k+1),1)'-Y(1,1);
    R(2,:) = Y(2:(k+1),2)'-Y(1,2);
    % the third row of R is all 1's, to ensure that the sum of weights is 1
    R(3,:) = ones(1,k);
    
    % alpha_p = R \ b; % particular solution
    
    % weights is the particular solution with minimal L2 norm 
    weights = R' * inv(R*R') * b;
    
    I(index) = i*ones(k,1);
    J(index) = idxs(2:(k+1),i);
    values(index) = weights;
    
    index = index + k;
end;

W = sparse(I,J,values, N, N);
Id=sparse(1:N,1:N,ones(1,N),N,N);
% W = W - Id;

% the eigenvectors 1, x, y should have eigenvalue 1
% thus, we look for the eigenvalues of W which are closest to 1
% the 1-1e-6 is due to the multiplicity of the eigenvalues (Matlab's bug)
[eigenvectors, lambda] = eigs(W,n_eigs,1-1e-6);
eigs_vals = diag(lambda, 0);

fprintf('Eigenvalues found by eigs: ...\n');
eigs_vals(1:n_eigs)

%%%%%%%% Plot histogram of the spectrum %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
a = sort(abs(eigs_vals-1));

figure;
bar([0:n_eigs-1],a);        
xlabel('i','FontSize', 14);
ylabel('|1-\lambda_i|', 'FontSize', 14);
%title('Spectrum of the center of mass operator (eigs)');
% axis([-1 10 0 0.015]);
axis auto;

%%%%%%%%%%%% Deal with the spectrum of a non-symmetric W %%%%%%%%%%%
%
% W is non-symmetric so its spectrum may be complex 
% Find which eigenvalues are real and which are complex
VECS = zeros(N,n_use);
is_real_part = 0;
n_real = 0;
n_complex = 0;
for i=1:n_use;
    if (abs(imag(eigs_vals(i))) > 0)
        n_complex = n_complex + 1;
        if (is_real_part == 0)
            % VECS(:,i) = real(V(:,eig_idxs(i)));
            VECS(:,i) = real(eigenvectors(:,i));
            is_real_part = 1;
        else
            % VECS(:,i) = imag(V(:,eig_idxs(i)));
            VECS(:,i) = imag(eigenvectors(:,i));
            is_real_part = 0;
        end;
        VECS(:,i) = VECS(:,i) / norm(VECS(:,i));
    else
        n_real = n_real + 1;
        VECS(:,i) = eigenvectors(:,i);
    end;
end;

PHI = zeros(N,n_use-1);
if (norm(VECS(:,1)-mean(VECS(:,1))) < eps)
    % the first eigenvector is the constant vector
    PHI = VECS(:,2:n_use);
else
    % Find a linear combination of the eigenvectors that give the constant one vector
    beta = VECS \ ones(N,1); % beta is a vector with n_use coefficients
    
    % Find n_use-1 linear combinations that are orthogonal to beta
    % those should also span x and y (but not 1!)
    Z = null(beta');
    PHI= VECS * Z;
    
    % subtract the mean so that all vectors are also orthogonal to 1
    for i=1:(n_use-1)
        PHI(:,i) = PHI(:,i) - mean(PHI(:,i));
    end;
end;

x_coeffs4 = VECS \ (x_data-mean(x_data))
x_coeffs3 = VECS(:,1:3) \ (x_data-mean(x_data)) 
X_error4 = norm(VECS * x_coeffs4 - (x_data-mean(x_data)))/sqrt(N)
X_error3 = norm(VECS(:,1:3) * x_coeffs3 - (x_data-mean(x_data)))/sqrt(N)


% In general, (x, y) = A*(phi1,phi2)
% r = A*phi
% therefore, 
% dist(i,j)=(r(i)-r(j))'*(r(i)-r(j) = (phi(i)-phi(j))' * A'*A * (phi(i)-phi(j))

% This gives linear equations for the matrix entries of A'*A
% We will use N*k linear equations: for each point we know the distances to
% its k nearest neighbors

% N*k equations in (n_use-1)^2 variables (e.g., 3 x 3 symmetric matrix entries)
equations = zeros(N*k,(n_use-1)^2);
rhs = zeros(N*k,1);

one_to_k = 1:k;
for i=1:N;
    index = 0;    
    rhs((i-1)*k+one_to_k) = dists(2:k+1,i); 
    for I=1:(n_use-1);
        vec1 = PHI(:,I);
        for J=1:(n_use-1);
            vec2 = PHI(:,J);
            index = index + 1;
            %(I,J)
            equations((i-1)*k+one_to_k,index) = ... 
              (vec1(i)-vec1(idxs(2:k+1,i))).*(vec2(i)-vec2(idxs(2:k+1,i))); 
        end;
    end;
end;

% Solve the following optimization problem:
% Find a positive semidefinite matrix ATA whose entries solve the equations
% in the least sqaures sense, that is,
% Find the best PSD matrix ATA that minimizes || equations * ATA - rhs ||


ATA = solve_sedumi(n_use-1, N*k, equations, rhs);

fprintf('SeDuMi found the following A^T*A: ...\n');
ATA

fprintf('Eigenvalues of A^T * A ...\n');
eig(ATA)

[U, S, V] = svd(ATA);

A = sqrt(S(1:2,:))*V';

fprintf('SVD gives A: ...\n');
A

% (x,y) = A*(phi_1, phi_2)

r_embed = PHI * A';
 
% figure;
% plot(r_embed(:,1), r_embed(:,2), '.');
% xlabel('\phi_1');
% ylabel('\phi_2');
% title('Center of mass kernel embedding');


%%%%%%%% Registration %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% r' = U * r_embed'
% r  = r_embed * U'
%
% where U is orthoganal
r = [x_data y_data];
T = r_embed\r; % least squares to find unitary+reflection trasformation T=U'

% T*T' % this should be 2 x 2 identity

% find best orthogonal approximation
[U,D,V] = svd(T);
Q = U * V';

r_embed = r_embed*Q; % rotating the embedding to the original (x,y,z) coordinates

r_embed(:,1) = r_embed(:,1) + mean(x_data);
r_embed(:,2) = r_embed(:,2) + mean(y_data);

figure;
subplot(2,2,1);
scatter(x_data, y_data, 7, x_data, 'filled');
% plot(x_data, y_data, '.');
xlabel('x', 'FontSize', 14);
ylabel('y', 'FontSize', 14);
axis equal;
axis tight;
title('US Cities','FontSize', 14);

subplot(2,2,2);
scatter(x_data, y_data, 7, y_data, 'filled');
% plot(x_data, y_data, '.');
xlabel('x', 'FontSize', 14);
ylabel('y', 'FontSize', 14);
axis equal;
axis tight;
title('US Cities', 'FontSize', 14);


subplot(2,2,3);
scatter(r_embed(:,1), r_embed(:,2), 7, x_data, 'filled');
% plot(r_embed(:,1), r_embed(:,2), '.');
xlabel('\phi_1', 'FontSize', 14);
ylabel('\phi_2', 'FontSize', 14);
axis equal;
axis tight;
title('Locally Rigid Embedding', 'FontSize', 14);

subplot(2,2,4);
scatter(r_embed(:,1), r_embed(:,2), 7, y_data, 'filled');
% plot(r_embed(:,1), r_embed(:,2), '.');
xlabel('\phi_1', 'FontSize', 14);
ylabel('\phi_2', 'FontSize', 14);
axis equal;
axis tight;
title('Locally Rigid Embedding', 'FontSize', 14);

figure;
scatter(x_data, y_data, 7, eigenvectors(:,4), 'filled');
% plot(x_data, y_data, '.');
xlabel('x', 'FontSize', 14);
ylabel('y', 'FontSize', 14);
axis equal;
axis tight;
colorbar;

MSE_x = norm(r_embed(:,1)-x_data)/sqrt(N)
MSE_y = norm(r_embed(:,2)-y_data)/sqrt(N)
