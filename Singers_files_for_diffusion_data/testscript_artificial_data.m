%This script creates artificial diffusion data to test Singer's "Local Rigid
%Embedding Algorithm. If the algorithm worked, pixels (2,2) would be closest
%to pixels (2,1) and (2,3) because the diffusion between these voxels is greatest.
%However, tihs is not occuring. 

%this is to initialize the SDPT3 solver.
if ~exist('OPTIONS')
    startup; 
end

% create the artificial data
% for this data the points in the middle row should be closer to each
% other, i.e. Data(2,1) should be close to Data(2,2) in the diffusion
% coordinates.
% The figure is the projection of the first three coordinates so it may
% look wrong. 
n = 3;
Data = zeros(n,n,2);
Data(ceil(n/2),:,1) = ones(1,n);
Data(:,:,1) = Data(:,:,1) + .05*randn(n);
Data(:,:,2) = .05*randn(n);

for a = -.05:-1:-5
    % create a distance matrix 
    %a = -1; %invert and scale parameter, must be negative. 
    %distance_matrix = construct_distance_matrix(Data);
    distance_matrix = construct_distance_matrix2(Data,a);

    % input this distance matrix into the modified Singer's algorithm
    k = 8; %number of neighbors for each data point.
    n_eigs = 6;
    n_use = 6; %number of the above eigenvectors to use. 
    noise = 0; 
    D = distance_matrix;
    r_embed = dave_sensor_uscities2(k,n_eigs,noise,n_use,D); %global coordinaes.
    D = squareform(pdist(r_embed));
    %error = D - distance_matrix; %this error does not make sense
    % since there are many unknow distances in distance_matrix.

    %the code below tests whether pixels 2, 5 and 8 are nearest neighbors.
    %if they are, then graph them. 
    [dists inds] = sort(D);
    if ((inds(2,5) == 8 && inds(3,5) == 2) || (inds(2,5) == 2 && inds(3,5) == 8))
        % plot the output
        figure(1)
        hold on;
        for k = 1:size(r_embed,1)
            [m j] = ind2sub([n n], k);
            scatter3(r_embed(k,1), r_embed(k,2), r_embed(k,3), 'k','.');
            s = ['(' num2str(m) ',' num2str(j) ')'];
            text(r_embed(k,1), r_embed(k,2), r_embed(k,3), s);
        end
        hold off;
        
        sprintf('a = %f',a) 
        pause
    end
end




    