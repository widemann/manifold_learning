% min sum (i<j) (||x_i - x_j||^2 - (d^_{ij})^2)^2 
% no objective function, the above is constraint. 
function global_coords = construct_global_coords_test(eigenvectors, distance_matrix)
D = distance_matrix;
phi = eigenvectors';
n = size(D,1);
dimension = n;
constr = full(sum(logical(D(:))))/2;
blk{1,1} = 's'; 
blk{1,2} = dimension;
AA = cell(1,constr);
b = zeros(constr,1);
cnt = 0;
for i = 1:n-1
    for j = i+1:n
        if (D(i,j)~=0)
            cnt = cnt + 1;
            AA{cnt} = (phi(:,i)-phi(:,j))*(phi(:,i)-phi(:,j))';
            b(cnt) = D(i,j)^2;
        end
    end
end

At(1) = svec(blk(1,:),AA);
C{1,1} = sparse(dimension,dimension);

[obj, X, y, Z] = sqlp(blk,At,C,b);

A = chol(X{1});
global_coords = A*phi;