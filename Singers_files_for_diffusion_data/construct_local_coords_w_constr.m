function local_coords = construct_local_coords_w_const(D)
% local_coords = construct_local_coords(distance_matrix)
% use semidefinite programming (SDPT3) to solve for the local coordinates of a 
% subset of the data points.
% input:
% points is the subset of the indices of the data points being used.
% distance_matrix is the matrix recording diffusion distances between data
% points. The points rows and columns of this matrix are being used. (make this global?)
% output:
% local_coords is the set of coordinates for the points"corresponding to the diffusion
% distances. 
% This is an implementation of step 3(c) of Singer's algorithm in "A remark on global
% positioning from local distances."

% David Widemann
% UC Davis, Nov. 2008

%D = distance_matrix(points,points);

% maybe this should be done.
% E = 1./D;
% E(E == inf) = 0;
% D = E;

n = size(D,2); %number of data points.
m = full(sum(logical(D(:)))/2); % number of edges, i.e. known distances. 

% formulate the SD problem.
blk{1,1} = 's';
blk{1,2} = n;
AA = cell(1,m);
b = zeros(m,1);
cnt = 0;
for i = 1:n-1
    for j = i+1:n
        if (D(i,j) ~= 0)
            cnt = cnt + 1;
            AA{cnt} = spconvert([i i 1; i j -1; j i -1; j j 1; n n 0]);
            b(cnt) = D(i,j) ^2;
        end
    end
end

At(1) = svec(blk(1,:),AA); 
C{1,1} = sparse(n,n);
blk{2,1} = 'l';
blk{2,2} = 2*m;
At{2,1} = [-speye(m),speye(m)];
C{2,1} = ones(2*m,1);

% solve the SD problem.
[obj,Xout,y,Z] = sqlp(blk,At,C,b);

% the coordinates are either in the lower triangular matrix of the Cholesky
% factorization.
[R p] = chol(Xout{1});
local_coords = R';
tol = 1e-2;
s = abs(sum(local_coords));
t = (s > tol);
local_coords = local_coords(:,t);

% testing accuracy
% DD = squareform(pdist(local_coords))
% 
% disp('')
% disp('original distance matrix');
% disp('')
% full(D)
% 
% distance_difference = D-DD