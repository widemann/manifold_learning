function compute_distance(i,j,idx,neighbors)
% compute_distance(i,j,idx,neighbors)
% compute the diffusion distance between voxel (i,j)
% and the voxels in neighbors. 
% this function computes (|<x,z>+<y,z>|)/2 for all the neighboring voxels.
% x = Data(i,j,:)
% y = Data(m,n,:) where (m,n) is a neighbor of (i,j)
% z = unit vector from (i,j) to (m,n)
% input: 
% (i,j) is the voxel (i,j)^th voxel. 
% idx is its index from the sub2indx function.
% neighbors are it's neighbors from the find_neighbors function.
% output:
% There is no output because D is global. 

% David Widemann
% UC Davis, Nov. 2008

global r c angles data D
x = squeeze(data(i,j,:));
for k = 1:size(neighbors,1)
    m = neighbors(k,1);
    n = neighbors(k,2);
    D(idx, sub2ind([r,c],m,n)) = ...
        abs((x'*angles(:,neighbors(k,3))) + squeeze(data(m,n,:))'*angles(:,neighbors(k,3)))/2;
end