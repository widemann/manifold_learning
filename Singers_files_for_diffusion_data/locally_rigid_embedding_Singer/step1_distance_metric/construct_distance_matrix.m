function distance_matrix = construct_distance_matrix(Data) %,a)
% distance_matrix = construct_distance_matrix(Data)
% input:
% Data is the diffusion data. It should be of the form Data(i,j,k,:) is diffusion measurements for 
% voxel (i,j,k)
% output:
% distance_matrix is a nxn matrix where n is the number of voxels that
% records the diffusion distance between a voxel and its neighbors. 

% David Widemann
% UC Davis, Nov. 2008

global data % diffusion data for each voxel (i,j,k), data(i,j,k,:)
global r % number of rows, i.e. voxels per column.
global c % number of columns, i.e. voxels per row.
global angles % diffusion directions to each voxel (see diagram) 
global D; % will become distance_matrix

% if a > 0
%     error('a must be negative');
% end

data = Data;
r = size(data,1); % number of voxels in x-direction,
c = size(data,2); % number of voxels in y-direction.

% angles between the voxels.              % 1 4 6
% the order is shown in the diagram:         \|/
theta = 0:pi/4:2*pi; theta(end) = [];     % 2-*-7
%                                            /|\
angles = [cos(theta); sin(theta)];        % 3 5 8
angles = [angles(:,4:6) angles(:,3) angles(:,7) angles(:,2) angles(:,1) angles(:,8) ];

% distance_matrix is the diffusion distance between the voxels. 
voxels = r*c;
D = sparse(voxels,voxels);

% compute the diffusion distance for each voxel
for idx = 1:size(D,1)
    % find neighbors
    [i j] = ind2sub([r c],idx);
    neighbors = find_neighbors(i,j);
    
    % compute distance for these neighbors
    compute_distance(i,j,idx,neighbors);
end

E = 1./D;
%m = max(D(:)); 
%T = m*exp(a*E); %"invert" the above distances. a should be negative.
%ind = 1:size(D,1)+1:size(D,1)^2; %set the diagonal to zero.
%T(ind) = 0;

E(E == Inf) = 0;

% % find the largest noninfinite entry in T.
% max_noninf = max(T(find(T ~= Inf)));
% % set the infinite values to max_noninf. 
% T(T==inf) = max_noninf;

distance_matrix = full(E);











