function global_coords = construct_global_coords(distance_matrix)
% global_coords = construct_global_coords(eigenvectors, data)
% use semidefinite programming to solve for global coordinates of the 
% data points.
% input:
% eigenvectors are the eigenvectors that span the null space of the S
% matrix in step3's LRE algorithm.
% k is the number of eigenvectors to use, one through k. 
% distance_matrix is the matrix recording diffusion distances between data
% points. (make this global?)
% output:
% global_coords is a set of coordinates for all the data.
% This is an implementation of step 5 of Singer's algorithm in "A remark on global
% positioning from local distances."
% The formulation for the SDP problems comes from Weinberger's "Graph
% Laplacian Regularization for Large-Scale semidefinite Programming"

% David Widemann
% UC Davis, Jan. 2009

n = size(distance_matrix,1); % number of data points. 
m = n*(n+1)/2; 

% building the quadratic block.
blk{1,1} = 'q';
blk{1,2} = m+1;
v = ones(m,1);
temp = -diag(v,1);
temp(end,:) = [];
At{1} = temp';
temp = zeros(m+1,1);
temp(1) = 1;
C{1} = temp;

% building the linear block.
blk{2,1} = 'l';
blk{2,2} = m;
temp_A = zeros(m);
temp_C = zeros(m,1);

for i = 1:n
    for j = i+1:n
        if (distance_matrix(i,j) ~= 0)
            ii = sub2ind([n n], i, i) - ( i*(i-1)/2);       %ii = ind(i,i);  
            ji = ii + (j-i);                                %ji = ind(j,i);  
            jj = sub2ind([n n], j, j) - ( j*(j-1)/2);       %jj = ind(j,j);  
            v = [ii ji jj]; 
            temp_A(v,v) = temp_A(v,v) + [1 -2 1; -2 4 -2; 1 -2 1];
            temp_C(v) = temp_C(v)-2*(distance_matrix(i,j)^2)*[1 -2 1]';
        end
    end
end
At{2} = temp_A;
C{2} = temp_C;
b = zeros(m,1);

% solving
[obj, X, y, Z] = sqlp(blk,At,C,b);

% creating the transformation matrix
y = X{2};
matrix_y = zeros(n);
j = 1;
for p = n:-1:1
    matrix_y(j:end,j) = y(1:p);
    y(1:p) = [];
    j = j+1;
end

matrix_y = matrix_y + matrix_y' - diag(diag(matrix_y),0);
