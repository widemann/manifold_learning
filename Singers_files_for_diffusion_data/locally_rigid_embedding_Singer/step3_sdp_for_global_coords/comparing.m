for power = -5:5
    dij = 10^power;
    distance_matrix = [0 dij; dij 0];
    
    %non-general
        m = 2;
        n = m*(m+1)/2;
        blk{1,1} = 'q';
        blk{1,2} = 4;
        temp = -diag(ones(3,1),1);
        temp(end,:) = [];
        At{1} = temp';
        C{1} = [1; 0; 0; 0];

        blk{2,1} = 'l';
        blk{2,2} = 3;
        At{2} = [1 -2 1; -2 4 -2; 1 -2 1];
        C{2} = -2*(dij^2)*[1;-2;1];

        b = zeros(3,1);

        [objj, XX, yy, ZZ] = sqlp(blk,At,C,b);
        
     %general
        n = size(distance_matrix,1); % number of data points. 
        m = n*(n+1)/2; 

        % building the quadratic block.
        blk{1,1} = 'q';
        blk{1,2} = m+1;
        v = ones(m,1);
        temp = -diag(v,1);
        temp(end,:) = [];
        At{1} = temp';
        temp = zeros(m+1,1);
        temp(1) = 1;
        C{1} = temp;

        % building the linear block.
        blk{2,1} = 'l';
        blk{2,2} = m;
        temp_A = zeros(m);
        temp_C = zeros(m,1);

        for i = 1:n
            for j = i+1:n
                if (distance_matrix(i,j) ~= 0)
                    ii = sub2ind([n n], i, i) - ( i*(i-1)/2);       %ii = ind(i,i);  
                    ji = ii + (j-i);                                %ji = ind(j,i);  
                    jj = sub2ind([n n], j, j) - ( j*(j-1)/2);       %jj = ind(j,j);  
                    v = [ii ji jj]; 
                    temp_A(v,v) = temp_A(v,v) + [1 -2 1; -2 4 -2; 1 -2 1];
                    temp_C(v) = temp_C(v)-2*(distance_matrix(i,j)^2)*[1 -2 1]';
                end
            end
        end
        At{2} = temp_A;
        C{2} = temp_C;
        b = zeros(m,1);

        % solving
        [obj, X, y, Z] = sqlp(blk,At,C,b);
        
       if (any(XX{2} ~= X{2}))
           sprintf('XX{2} = %1.12f %1.12f %1.12f', XX{2})
           sprintf('X{2} = %1.12f %1.12f %1.12f', X{2})
           sprintf('dij = %1.12f', 10^power)
           pause;
       end
end

           
           
           
           
           
           