%testing Singer's algorithm
N = 10;
p = 2;
X = randn(N,p);
distance_matrix = squareform(pdist(X));

[a b] = sort(distance_matrix);

error = zeros(N-1,1);
eigenvectors = LRE_algorithm(distance_matrix);
global_coords = construct_global_coords(eigenvectors, distance_matrix);
D2 = squareform(pdist(global_coords'));
error(1) = max(max(abs(distance_matrix-D2)))
distance_matrix-D2
pause;

for k = 0:4
    for j = 1:N
        distance_matrix(j ,b(end-k, j)) = 0;
        distance_matrix(b(end-k, j), j) = 0;
    end
    eigenvectors = LRE_algorithm(distance_matrix);
    global_coords = construct_global_coords(eigenvectors, distance_matrix);
    D2 = squareform(pdist(global_coords'));
    error(k+2) = max(max(abs(distance_matrix-D2)))
    distance_matrix-D2
    pause;
end
