function eigenvectors = LRE_algorithm(distance_matrix)
% eigenvectors = LRE_algorithm(distance_matrix)
% input:
% distance_matrix is the diffusion distances between the data points.
% output:
% eigenvectors are the eigenvectors of the S-matrix below. 
% This is an implementation of steps 2-4 of Singer's algorithm in "A remark on global
% positioning from local distances."

% David Widemann
% UC Davis, Nov. 2008
r = size(distance_matrix,1);

%form Ri for each data point, i.
%create Si and add it to S. 
S = zeros(r);
for i = 1:r
    %create Ri. 
    [a neighbors] = find(distance_matrix(i,:)~=0);
    a = (neighbors < i);
    points = [neighbors(a), i, neighbors(~a)];
    local_coords = construct_local_coords(points, distance_matrix);
    Ri = local_coords'; 
    Ri = [Ri;ones(1,size(Ri,2))]; 
    %solve for the nullspace. 
    Y = null(Ri);
    Z = zeros(r,size(Y,2));
    Z(points,:) = Y;
    Si = Z*Z';
    S = S + Si;
end

eig1 = ones(size(S,2),1)/norm(ones(size(S,2),1));
S = S - eig1*eig1';
[eigenvectors D] = eig(S);

tol = 1e-2;
a = diag(D);
v = 2:size(eigenvectors,2);%(abs(a) < tol);
eigenvectors = eigenvectors(:,v)'; %[eigenvectors(:,v) zeros(size(eigenvectors,1),size(v,1)-sum(v))];

%self-checks
% eigenvectors'*eigenvectors
% S*eigenvectors - eigenvectors*D

% plot the new coordinates. 
% coordinates = eigenvectors(:,2:end)';
% figure, scatter(coordinates(1,:),coordinates(2,:),'filled')
% figure,
% scatter3(coordinates(1,:),coordinates(2,:),coordinates(3,:),'filled')