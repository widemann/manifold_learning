function neighbors = find_neighbors(i,j)
% neighbors = find_neighbors(i,j)
% finds the valid neighbors of voxel (i,j) that we want to compute the
% distance between
% angles between the voxels.              % 1 4 6
% the order is shown in the diagram:         \|/
                                          % 2-*-7
%                                            /|\
                                          % 3 5 8
   
% David Widemann
% UC Davis, Nov. 2008

global r c

neighbors = [i-1, j-1; i, j-1; i+1, j-1; i-1, j; i+1, j; i-1, j+1; i, j+1; i+1, j+1]';
neighbors = [neighbors; 1:8]';
for k = size(neighbors,1):-1:1
    if ((neighbors(k,1)*neighbors(k,2) == 0) || (neighbors(k,1) > r) || (neighbors(k,2) > c))
        neighbors(k,:) = [];
    end
end
