%testing Weinberger's algorithm
clear
N = 10;
p = 2;
X = randn(N,p);
distance_matrix = squareform(pdist(X)).^2;
dm = distance_matrix;

[a b] = sort(distance_matrix);
error = zeros(N-1,1);
%eigenvectors = LRE_algorithm(distance_matrix);
[y_dist_mat,det] = fastmvu(distance_matrix,2,'leigsdim',8,'eta',1e-04,'maxiter',500);
%[y_dist_mat,det] = fastmvu_Weigenvectors(distance_matrix,2, eigenvectors,'leigsdim',9,'eta',1e-04,'maxiter',500);
D2 = squareform(pdist(y_dist_mat')).^2;
error(1) = max(max(abs(dm-D2)))
distance_matrix-D2

pause;

% for k = 0:10
%     for j = 1:N
%         distance_matrix(j ,b(end-k, j)) = 0;
%         distance_matrix(b(end-k, j), j) = 0;
%     end
% end
eigenval = [];
for k = 0:4
    eigenval = [eigenval eig(distance_matrix)];
    for j = 1:N
        distance_matrix(j ,b(end-k, j)) = 0;
        distance_matrix(b(end-k, j), j) = 0;
    end
    %eigenvectors = LRE_algorithm(distance_matrix);
    [y_dist_mat,det] = fastmvu(distance_matrix,2,'leigsdim',8,'eta',1e-04,'maxiter',500);
    %[y_dist_mat,det] = fastmvu_Weigenvectors(distance_matrix,2, eigenvectors,'leigsdim',9,'eta',1e-04,'maxiter',500);
    D2 = squareform(pdist(y_dist_mat')).^2;
    error(k+2) = max(max(abs(dm-D2)))
    dm-D2
    pause;
end


%testing Singer's algorithm
clear
N = 10;
p = 2;
X = randn(N,p);
distance_matrix = squareform(pdist(X)).^2;

[a b] = sort(distance_matrix);
error = zeros(N-1,1);
eigenvectors = LRE_algorithm(distance_matrix);
[y_dist_mat,det] = fastmvu_Weigenvectors(distance_matrix,2, eigenvectors,'leigsdim',9,'eta',1e-04,'maxiter',500);
D2 = squareform(pdist(y_dist_mat')).^2;
error(1) = max(max(abs(distance_matrix-D2)))
distance_matrix-D2
dm = distance_matrix;
pause;

for k = 0:5
    for j = 1:N
        distance_matrix(j ,b(end-k, j)) = 0;
        distance_matrix(b(end-k, j), j) = 0;
    end
    eigenvectors = LRE_algorithm(distance_matrix);
    [y_dist_mat,det] = fastmvu_Weigenvectors(distance_matrix,2, eigenvectors,'leigsdim',9,'eta',1e-04,'maxiter',500);
    D2 = squareform(pdist(y_dist_mat')).^2;
    error(k+2) = max(max(abs(dm-D2)))
    dm-D2
    pause;
end


