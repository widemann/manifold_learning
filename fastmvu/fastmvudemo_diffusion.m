% echo on;
% 
% % generating random data set
% x=load('uscities.txt');
% N=size(x,2);
% 
% figure(1);
% clf;
% plot(x(1,:),x(2,:),'k.');
% set(gca,'FontSize',18);
% title('INPUT');
% ax=axis;
% drawnow;

% connecting data points within radius of 0.1 and corrupting it with 10% noise
% D=dissimilar(x,0.05^2,30,0.1);
noise = 0.05;
n = 3;
m = 5;
Data = noise*randn(n,m,2) + zeros(n,m,2);
Data(2,:,1) = 1;
D = construct_distance_matrix(Data);
tol = 1e-2;
D(D<tol) = 0;
E = 1./D;
E(E==inf) = 0;
D = E;
[r c t] = size(Data);
x = 1:r;
y = 1:c;
[X Y] = meshgrid(x,y);
X = [Y(:) X(:)];
x = X';

figure(2);
clf;
plot(x(1,:),x(2,:),'k.');
set(gca,'FontSize',18);
title('epsilon-GRAPH');
ax=axis;
hold on;
gplot(D,x');
drawnow;

% run fastmvu
[y,det]=fastmvu(D,2,'leigsdim',8,'eta',1e-04,'maxiter',500);

% plot output
figure(3);
clf;
hold on;
%y(1,:)=y(1,:).*sign(y(1,floor(size(D,1)/2)));
%y(2,:)=y(2,:).*sign(y(2,floor(size(D,1)/2)));
for i = 1:size(y,2)
    e = .1;
    plot(y(1,:),y(2,:),'k.');
    text(y(1,i)+e, y(2,i)+e, num2str(i));
end
set(gca,'FontSize',18);
title('OUTPUT');
hold off;
%axis(ax);








